import React from 'react';   
import { Container, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';  
import axios from 'axios'  
import './AddProducts.css'
class Edit extends React.Component {  
    constructor(props) {  
        super(props)  
     
    this.onChangeName = this.onChangeName.bind(this);  
    this.onChangeRollNo = this.onChangeCompany.bind(this);  
    this.onChangeClass = this.onChangePrice.bind(this);  
    this.onChangeAddress = this.onChangeDescription.bind(this);  
    this.onChangeAddress = this.onChangeCategory.bind(this);  
    this.onSubmit = this.onSubmit.bind(this);  
  
         this.state = {  
            Name: '',
            Company: '',  
            Price: '',  
            Description: '',  
            Category: ''  
  
        }  
    }  
  
  componentDidMount() {  
      axios.get('https://localhost:44340//api/product/ProductdetailById?id='+this.props.match.params.id)  
          .then(response => {  
              this.setState({   
                Name: response.data.Name,   
                Company: response.data.Company,  
                Price: response.data.Price, 
                Category: response.data.Category, 
                Description: response.data.Description });  
  
          })  
          .catch(function (error) {  
              console.log(error);  
          })  
    }  
  
  onChangeName(e) {  
    this.setState({  
        Name: e.target.value  
    });  
  }  
  onChangeCompany(e) {  
    this.setState({  
        Company: e.target.value  
    });    
  }  
  onChangePrice(e) {  
    this.setState({  
        Price: e.target.value  
    });  
}  
    onChangeCategory(e) {  
        this.setState({  
            Category: e.target.value  
        });  
  }  
  onChangeDescription(e) {  
    this.setState({  
        Description: e.target.value  
    });  
}  
  
  onSubmit(e) {  
    debugger;  
    e.preventDefault();  
    const obj = {  
        Id:this.props.match.params.id,  
      Name: this.state.Name,  
      Company: this.state.Company,  
      Price: this.state.Price,  
      Category: this.state.Category, 
      Description: this.state.Description   
  
    };  
    axios.post('http://localhost:4000/Api/Product/AddotrUpdateproduct/', obj)  
        .then(res => console.log(res.data));  
        debugger;  
        this.props.history.push('/Productlist')  
  }  
    render() {  
        return (  
            <Container className="App">  
  
             <h4 className="PageHeading">Update Product Informations</h4>  
                <Form className="form" onSubmit={this.onSubmit}>  
                    <Col>  
                        <FormGroup row>  
                            <Label for="name" sm={2}>Name</Label>  
                            <Col sm={10}>  
                                <Input type="text" name="Name" value={this.state.Name} onChange={this.onChangeName}  
                                placeholder="Enter Name" />  
                            </Col>  
                        </FormGroup>  
                        <FormGroup row>  
                            <Label for="Password" sm={2}>Company</Label>  
                            <Col sm={10}>  
                                <Input type="text" name="Company" value={this.state.Company} onChange={this.onChangeCompany} placeholder="Enter Company" />  
                            </Col>  
                        </FormGroup>  
                         <FormGroup row>  
                            <Label for="Password" sm={2}>Price</Label>  
                            <Col sm={10}>  
                                <Input type="text" name="Price" value={this.state.Price} onChange={this.onChangePrice} placeholder="Enter Price" />  
                            </Col>  
                        </FormGroup>  
                         <FormGroup row>  
                            <Label for="Password" sm={2}>Category</Label>  
                            <Col sm={10}>  
                                <Input type="text" name="Category"value={this.state.Category} onChange={this.onChangeCategory} placeholder="Enter Category" />  
                            </Col>  
                        </FormGroup>   
                        <FormGroup row>  
                            <Label for="Password" sm={2}>Description</Label>  
                            <Col sm={10}>  
                                <Input type="text" name="Description"value={this.state.Description} onChange={this.onChangeDescription} placeholder="Enter Description" />  
                            </Col>  
                        </FormGroup>  
                    </Col>  
                    <Col>  
                        <FormGroup row>  
                            <Col sm={5}>  
                            </Col>  
                            <Col sm={1}>  
                          <Button type="submit" color="success">Submit</Button>{' '}  
                            </Col>  
                            <Col sm={1}>  
                                <Button color="danger">Cancel</Button>{' '}  
                            </Col>  
                            <Col sm={5}>  
                            </Col>  
                        </FormGroup>  
                    </Col>  
                </Form>  
            </Container>  
        );  
    }  
  
}  
  
export default Edit;  