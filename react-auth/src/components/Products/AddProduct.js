import React from 'react';  
import axios from 'axios';  
import './AddProducts.css'  
import { Container, Col, Form, Row, FormGroup, Label, Input, Button } from 'reactstrap';  
class Addproduct extends React.Component{  
constructor(props){  
super(props)  
this.state = {  
    Name: '',
    Company: '',  
    Price: '',  
    Description: '',  
    Category: '' 
}
}   
Addproduct=()=>{  
  axios.post('https://localhost:44340//api/product/AddotrUpdateproduct/', {Name:this.state.Name,Company:this.state.Company,  
  Price:this.state.Price, Category:this.state.Category, Description:this.state.Category})  
.then(json => {  
if(json.data.Status==='Success'){  
  console.log(json.data.Status);  
  alert("Data Save Successfully");  
this.props.history.push('/Productlist')  
}  
else{  
alert('Data not Saved');  
debugger;  
this.props.history.push('/Productlist')  
}  
})  
}  
   
handleChange= (e)=> {  
this.setState({[e.target.name]:e.target.value});  
}  
   
render() {  
return (  
   <Container className="App">  
    <h4 className="PageHeading">Enter Product Informations</h4>  
    <Form className="form">  
      <Col>  
        <FormGroup row>  
          <Label for="name" sm={2}>Name</Label>  
          <Col sm={10}>  
            <Input type="text" name="Name" onChange={this.handleChange} value={this.state.Name} placeholder="Enter Name" />  
          </Col>  
        </FormGroup>  
        <FormGroup row>  
          <Label for="address" sm={2}>Company</Label>  
          <Col sm={10}>  
            <Input type="text" name="Company" onChange={this.handleChange} value={this.state.Company} placeholder="Enter Company" />  
          </Col>  
        </FormGroup>  
        <FormGroup row>  
          <Label for="Password" sm={2}>Price</Label>  
          <Col sm={10}>  
            <Input type="text" name="Price" onChange={this.handleChange} value={this.state.Price} placeholder="Enter Price" />  
          </Col>  
        </FormGroup>  
        <FormGroup row>  
          <Label for="Password" sm={2}>Category</Label>  
          <Col sm={10}>  
            <Input type="text" name="Category" onChange={this.handleChange} value={this.state.Category} placeholder="Enter Category" />  
          </Col>  
        </FormGroup>  
        <FormGroup row>  
          <Label for="Password" sm={2}>escription</Label>  
          <Col sm={10}>  
            <Input type="text" name="Description" onChange={this.handleChange} value={this.state.Description} placeholder="Enter Description" />  
          </Col>  
        </FormGroup>  
      </Col>  
      <Col>  
        <FormGroup row>  
          <Col sm={5}>  
          </Col>  
          <Col sm={1}>  
          <button type="button" onClick={this.AddProduct} className="btn btn-success">Submit</button>  
          </Col>  
          <Col sm={1}>  
            <Button color="danger">Cancel</Button>{' '}  
          </Col>  
          <Col sm={5}>  
          </Col>  
        </FormGroup>  
      </Col>  
    </Form>  
  </Container>  
);  
}  
   
}  
   
export default Addproduct;  